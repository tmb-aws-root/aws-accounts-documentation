**Markdown Formatted Text**
```
"Issue Title"

"Issue Description"

---
**Requirements**
- [ ] Commit an architecture diagram to the document repository

[comment]: <> (markdown files provide a better user-experience when viewing important documentation.)

[comment]: <> (The below command sets the issue's time estimate to 2 weeks, 1 day, 5 hours, and 20 minutes ...)
[comment]: <> (/estimate 2w 1d 5h 20m)

[comment]: <> (The command below increments the issue's time spent tracking by 1 day)
[comment]: <> (/spent 1d)
```

**Rendered Text**

"Issue Title"

"Issue Description"

---
**Requirements**
- [ ] Commit an architecture diagram to the document repository

[comment]: <> (markdown files provide a better user-experience when viewing important documentation.)

[comment]: <> (The below command sets the issue's time estimate to 2 weeks, 1 day, 5 hours, and 20 minutes ...)
[comment]: <> (/estimate 2w 1d 5h 20m)

[comment]: <> (The command below increments the issue's time spent tracking by 1 day)
[comment]: <> (/spent 1d)